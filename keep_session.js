if(Drupal.jsEnabled) {
  $(document).ready(function() {
    $("iframe#keep_session_iframe").remove();
    var timer = setInterval(function() {
      $.get(Drupal.settings.keep_session.json_url, function(data) {});
    }, Drupal.settings.keep_session.refresh_interval);
  });
}
